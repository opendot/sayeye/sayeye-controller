# Sayeye
 Sayeye Suite is a set of software co-designed for people suffering from Rett's syndrome characterized by an innovative way of interaction between care-giver and care-receiver, both equipped with an instrument on their own device, and through the use of an eyetracker (which allows you to track the look of the subject and to determine which point on the screen is watching).


Sayeye is an open source software and accessible to everyone, co-designed by designers, developers, university researchers, families and therapists. Sayeye promotes communication and interactions between  care-givers and care-receivers.
The system integrates different technologies (mobile applications, cloud services and adaptive algorithms) to provide an innovative, comprehensive and easy-to-use service.


The software is a fork of the project Amelie. Amelie was born from an idea of Associazione Italiana Rett - AIRETT Onlus and Opendot S.r.l., and was designed and developed by Opendot S.r.l., with the essential contribution of Associazione Italiana Rett - AIRETT Onlus.

This repository hosts the background process controlling the status of the Sayeye server on desktop PCs.


# sayeye-controller

This component of the Sayeye suite acts as daemon for the sayeye rails server, loads it at startup, and enables the user to open the interface (sayeye-communicator).
It is a tray icon application, the menu can be accessed by clicking on it with a right click of the mouse (or a long touch). The icon reveals the current state of the server (running or down). 

Technologies
---

The controller is developed as a .NET c# project, and depends on the following nuGet packages:

- Tobii.Research.x64


Compile and run
---

- Open the project with visual studio 2017, install the required nuGet packages and run the compilation (Debug or Release, depending on your needs).
- Once compiled, in the destination folder open the file "sayeyeController.exe.config"
- in the appSettings section, modify the value with key "programFolder" to address the installation folder of the Sayeye suite (this step is performed automatically by the Sayeye installer)
- run the executable

